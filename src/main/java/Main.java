import java.util.Comparator;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {

        // 1.
        // Definiera ett eget funktionellt interface, som tar in en sträng och returnerar en
        // sträng. Implementera det så att ord som sänds in returneras omvandlade till
        // enbart stora bokstäver


        UppercaseInterface uppercaseInterface = String::toUpperCase;

        System.out.println(
                uppercaseInterface.apply("studiegrupp två")
        );

        // 2.
        // Skriv 3 olika Lambda-uttryck, som inte går att omvandla till
        // metodreferensform.


        BiFunction<Integer, Integer, Integer> noMethodReferenceOne = (n1, n2) -> n1 * n2;
        Function<Integer, Boolean> noMethodReferenceTwo = n1 -> n1 != 5;
        Function<Object, String> noMethodReferenceThree = o -> o.toString().toUpperCase();

        // 3.
        // Skapa en lista med 5 olika siffror. Med hjälp av Streams, returnera vad värdet
        // blir om man summerar (plussar) ihop dem.

        System.out.println(
                List.of(1, 2, 3, 4, 5)
                        .stream()
                        .reduce(0, Integer::sum)
        );

        // 4.
        // Skapa en lista med 5 anställda, som har fält för namn, ålder och lön. Printa
        // den som är näst yngst av de 3 som har högst lön.

        List<Employee> employees = List.of(
                new Employee("Gunnar", 23, 32000),
                new Employee("Göran", 42, 22000),
                new Employee("Stina", 33, 44500),
                new Employee("Tuva", 21, 27000),
                new Employee("Kim", 37, 37000)
        );
        System.out.println(
                employees
                        .stream()
                        .sorted(Comparator.comparing(Employee::getSalary).reversed())
                        .limit(3)
                        .sorted(Comparator.comparing(Employee::getAge))
                        .skip(1)
                        .limit(1)
                        .toList()
        );

        // 5.
        // Skapa en lista med 5 olika ord. Med hjälp av Streams, returnerar hur många
        // tecken som orden innehöll i snitt

        System.out.println(
                List.of("Vattenflaska", "Laptopladdare", "Kaffekopp", "Musmatta", "Tegelhus")
                        .stream()
                        .mapToDouble(String::length)
                        .summaryStatistics()
                        .getAverage()
        );

        // 6.
        // Du och 7 andra vänner ska dyka från klipporna. Ni bestämmer er för att ha en
        // tävling - vem dyker snyggast - där de 7 som inte hoppar ger poäng 1-10. För
        // att inte påverkas av om någon har en väldigt avvikande åsikt, väljer ni att
        // plocka bort de två högsta och de två lägsta domarpoängen, för att sedan
        // räkna ut snittet av kvarvarande.
        // Implementera interfacet Function<T, R>, så att din implementation kan ta in
        // en List<Integer> och returnera en Double. Din implementation skall sedan,
        // med hjälp av stream, returnera slutpoängen enligt beskrivningen ovan.

        List<
                Integer> score = List.of(8, 4, 3, 7, 9, 5, 6);

        Function<List<Integer>, Double> calculatePoints = list -> list
                .stream()
                .sorted(Comparator.naturalOrder())
                .limit(5)
                .skip(2)
                .mapToDouble(x -> x)
                .summaryStatistics()
                .getAverage();

        System.out.println(
                calculatePoints.apply(score)
        );

        // 7.
        // Kör en Stream, med siffrorna 1 till 10. I slutändan skall det för varje siffra
        // printas ut om talet är jämnt eller inte (t,ex “1 - Ojämnt”, “2 - Jämnt”)
                Stream.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
                        .forEach(n -> {
                            if (n % 2 != 0)
                                System.out.println(n + " - Ojämnt");
                            else
                                System.out.println(n + " - Jämnt");
                        });

        // VI HANN INTE GÖRA FÄRDIGT NEDAN

        // 8.
        // Skapa en klass Month, som innehåller dels ett fält för månadens namn, dels
        // en List<Double> på ett antal uppmätta temperaturer under den månaden. I
        // din main-klass, skapa i sin tur en lista med årets tolv sådana månader. Från
        // den listan, skapa en ström som returnerar en Map med månadsnamnet som
        // key och snittemperaturer som value

        List<Month> months = List.of(
                new Month("Januari", List.of(-1.0, 2.2, 3.0)),
                new Month("Februari", List.of(-5.0, 7.2, 3.0)),
                new Month("Mars", List.of(5.0, 3.2, 4.4)),
                new Month("April", List.of(10.0, 7.2, 5.4)),
                new Month("Maj", List.of(20.0, 15.2, 19.0)),
                new Month("Juni", List.of(29.0, 25.2, 30.5)),
                new Month("Juli", List.of(15.0, 17.2, 23.0)),
                new Month("Augusti", List.of(30.0, 32.2, 25.0)),
                new Month("September", List.of(15.0, 16.2, 18.0)),
                new Month("Oktober", List.of(10.0, 12.2, 13.0)),
                new Month("November", List.of(-1.0, -3.2, 5.0)),
                new Month("December", List.of(-1.0, 2.2, 3.0))
        );
        System.out.println(
        months
                .stream()
                .collect(Collectors.toMap(
                        Month::getMonth,
                        Month::getTemperature
                        )
                )
        );

    }

}
