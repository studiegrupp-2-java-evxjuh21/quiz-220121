@FunctionalInterface
public interface UppercaseInterface {

    String apply(String str);

}
